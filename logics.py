import csv
import os

def getting_names_from_csv(file_path):
# reading the csv file and getting names into the list 
    names = []
  # try:
    with open(file_path, "r") as file:
        csvreader = csv.reader(file)
        next(csvreader)
        for row in csvreader:
            names.append({"name":row[0],"after":convert_days_format(row[1])})
    return names 
  # except 

def convert_days_format(day):
    match day.lower():
        case "sunday":
            return 0
        case "monday":
            return 1
        case "tuesday":
            return 2
        case "wednesday":
            return 3
        case "thursday":
            return 4
        case "friday":
            return 5
        case "saturday":
            return 6
        case _:
            return -1

def counting_names(names):
    names_size = len(names)
    return(names_size)

    
def put_in_smallest_day(person, week):
    # Initialization
    min_len = float('inf')
    min_day = -1
    # Loop through each list in the week
    for i, lst in enumerate(week):
        # Check if the current list is smaller than the smallest list found so far
        if len(lst) < min_len and i != person["after"]:
            min_len = len(lst)
            min_day = i
    return min_day

def devide_to_days(names,names_size):
    is_sheerit = False
    if names_size%7 != 0:
        is_sheerit = True
    week = [[],[],[],[],[],[],[]]
    for person in names:
        day = put_in_smallest_day(person,week)
        week[day].append(person["name"])
    return(week,is_sheerit)
                    
                    

def devide_to_days_less_then_seven(names,names_size):
    week = []
    for i in range(7):
        week.append([names[i%names_size]["name"]])
    return (week,False)

def new_csvfile_regular(john_week):
    # creating a new csv file to save the data of each person on different days
    csvfile = open("plugat_hagana.csv", "w" , newline="", encoding= "utf-8-sig") # encoding= "utf-8-sig" = making excel understand Hebrew.
    headers = ["ראשון","שני","שלישי","רביעי","חמישי","שישי","שבת"]
    writer = csv.writer(csvfile)
    writer.writerow(headers)
    transposed_data = list(zip(*john_week))
    for row in transposed_data:
        writer.writerow(row)
    csvfile.close()

def map_dictonary_to_names(first_list):
    second_list= []
    for m in first_list:
        second_list.append(m["name"])
    return second_list
        
def new_csvfile_sheerit(names):
    sheerit_size = len(names)%7
    sheerit_names = names[-sheerit_size::]
    new_names = ['','','','','','','']
    for person in sheerit_names:
        day = put_in_smallest_day(person,new_names)
        new_names[day] = person["name"]
    csvfile = open("plugat_hagana.csv", "a" , newline="", encoding= "utf-8-sig")
    writer = csv.writer(csvfile)
    writer.writerow(new_names)
    csvfile.close()

def main(file_path):
    print("Beggining Script")
    print("Reading names from CSV file")
    names = getting_names_from_csv(file_path)
    names_size = counting_names(names)
    john_week,is_sheerit = devide_to_days(names,names_size)
    new_csvfile_regular(john_week)
    if is_sheerit:
        new_csvfile_sheerit(names)
    current_path = os.getcwd()
    print(f"File Created to: {current_path}")
    return current_path



if __name__ == "__main__":
    main()