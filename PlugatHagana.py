import tkinter as tk
import tkinter.font as tkFont
from tkinter import filedialog, PhotoImage
import logics

class App:
    def __init__(self, root):
        #setting title
        root.title("Plugat Hagana")
        #setting window size
        width=700
        height=500
        screenwidth = root.winfo_screenwidth()
        screenheight = root.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        root.geometry(alignstr)
        root.resizable(width=False, height=False)

        GLabel_413=tk.Label(root)
        GLabel_413["activebackground"] = "#232324"
        GLabel_413["anchor"] = "center"
        GLabel_413["bg"] = "#232324"
        GLabel_413["borderwidth"] = "0px"
        GLabel_413["disabledforeground"] = "#f8f8f8"
        ft = tkFont.Font(family='Heebo',size=28)
        GLabel_413["font"] = ft
        GLabel_413["fg"] = "#eaebea"
        GLabel_413["justify"] = "center"
        GLabel_413["text"] = "Plugat Hagana"
        GLabel_413["relief"] = "ridge"
        GLabel_413.place(x=0,y=0,width=698,height=80)

        GLabel_941=tk.Label(root)
        GLabel_941["bg"] = "#232324"
        ft = tkFont.Font(family='Heebo',size=10)
        GLabel_941["font"] = ft
        GLabel_941["fg"] = "#ededed"
        GLabel_941["justify"] = "center"
        GLabel_941["text"] = ""
        GLabel_941.place(x=0,y=80,width=698,height=415)

        GLabel_343=tk.Label(root)
        GLabel_343["bg"] = "#232324"
        ft = tkFont.Font(family='Heebo',size=18)
        GLabel_343["font"] = ft
        GLabel_343["fg"] = "#eaebea"
        GLabel_343["justify"] = "center"
        GLabel_343["text"] = "הוסף את הקובץ כאן"
        GLabel_343.place(x=500,y=90,width=199,height=62)

        uplaod_button=tk.Button(root)
        uplaod_button["activebackground"] = "#826c9e"
        uplaod_button["anchor"] = "center"
        uplaod_button["bg"] = "#b88bf2"
        ft = tkFont.Font(family='Heebo',size=12)
        uplaod_button["font"] = ft
        uplaod_button["fg"] = "#39184b"
        uplaod_button["justify"] = "center"
        uplaod_button["text"] = "העלאת קובץ"
        uplaod_button["relief"] = "groove"
        uplaod_button.place(x=390,y=110,width=110,height=30)
        uplaod_button["command"] = self.uplaod_button_command

        download_button=tk.Button(root)
        download_button["bg"] = "#14d7c4"
        ft = tkFont.Font(family='Heebo',size=14)
        download_button["font"] = ft
        download_button["fg"] = "#000000"
        download_button["justify"] = "center"
        download_button["text"] = "הורד קובץ"
        download_button.place(x=280,y=350,width=145,height=51)
        download_button["command"] = self.download_button_command
        
        self.label_path = tk.Label(root, text="Selected File: None", bg="#232324",fg="#eaebea")
        self.label_path.place(x=0,y=300,width=698,height=25)
        
        self.success_label = tk.Label(root, text="", bg="#232324",fg="#eaebea")
        self.success_label.place(x=0,y=425,width=698,height=25)
        
        self.file_path = ""

    def uplaod_button_command(self):
        self.file_path = filedialog.askopenfilename(title="Select a file")
        if self.file_path:
            # Do something with the selected file path (e.g., display it in a label)
            self.label_path.config(text=f"Selected File: {self.file_path}")


    def download_button_command(self):
        current_path = logics.main(self.file_path)
        self.success_label.config(text=f"File Downloaded Successfully: {current_path}/plugat_hagana.csv")

if __name__ == "__main__":
    root = tk.Tk()
    app = App(root)
    root.mainloop()
